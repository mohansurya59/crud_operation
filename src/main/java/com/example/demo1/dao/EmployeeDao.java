package com.example.demo1.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.demo1.model.Employee;
import com.example.demo1.repository.EmployeeRepository;

@Component
public class EmployeeDao {
	@Autowired
	EmployeeRepository employeeRepository;

	public List<Employee> getMapping() {
		return employeeRepository.findAll();
	}

	public Employee postEmployee(Employee emp) {
		return employeeRepository.save(emp);
	}
	
	public Employee updateEmployee(long id, Employee emp) {
		
		Employee emp1=employeeRepository.getOne(id);
		
		emp1.setName(emp.getName());
		emp1.setSalary(emp.getSalary());
		return employeeRepository.save(emp1);
	}

	public void deleteEmployee(long id) {
		employeeRepository.deleteById(id);
		
	}

	
	
	

}
