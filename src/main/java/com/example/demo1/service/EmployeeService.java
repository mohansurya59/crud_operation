package com.example.demo1.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo1.dao.EmployeeDao;
import com.example.demo1.model.Employee;
@Service
public class EmployeeService {
	@Autowired
	EmployeeDao employeeDao;

	@GetMapping("/emp")
	public List<Employee> getMapping() {
	
		return employeeDao.getMapping();
	}

	public Employee postEmployee(Employee emp) {
		// TODO Auto-generated method stub
		return employeeDao.postEmployee(emp);
	}

	public void deleteEmployee(long id) {
		 employeeDao.deleteEmployee(id);
		
	}

	public Employee updateEmployee(long id, Employee emp) {
		// TODO Auto-generated method stub
		return employeeDao.updateEmployee(id,emp);
	}
	
	
	
	

	

}
