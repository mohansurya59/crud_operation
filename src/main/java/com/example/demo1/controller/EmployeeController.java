package com.example.demo1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo1.model.Employee;
import com.example.demo1.service.EmployeeService;



@RestController
@RequestMapping("/v1")
public class EmployeeController {
	@Autowired
	EmployeeService employeeService;
	
	@GetMapping("/employee")
	public List<Employee> getMapping(){
		return employeeService.getMapping();
		
	}
	@PostMapping("/employee1")
	public Employee postEmployee(@RequestBody Employee emp) {
		return employeeService.postEmployee(emp);
	}
	@PutMapping("emp2/{id}")
	public Employee updateEmployee(@PathVariable("id") long id,@RequestBody Employee emp) {
		return employeeService.updateEmployee(id,emp);
	}
	@DeleteMapping("/employee/{id}")
	public void deleteEmployee(@PathVariable("id") long id)
	{
		employeeService.deleteEmployee(id);
	}
	
}










